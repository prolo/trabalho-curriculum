import React from "react";
import './styles.css';

import Button from '../Button';

class EditCurriculum extends React.Component {
    constructor() {
        super()

        this.state = {
            curriculum: {
                name: '',
                email: '',
                celphone: '',
                jobRole: '',
                description: '',
            },
            validation: {
                form: null,
                name: null,
                email: null,
                celphone: null,
                jobRole: null,
                description: null,
            }
        }

        this.formRef = React.createRef();
        this.nameRef = React.createRef();
        this.emailRef = React.createRef();
        this.celphoneRef = React.createRef();
        this.jobRoleRef = React.createRef();
        this.descriptionRef = React.createRef();
    }

    componentDidMount() {
        if (this.props.curriculum) this.setState({ curriculum: this.props.curriculum })
    }

    handleSave() {
        this.props.setCurriculum(this.state.curriculum);
        localStorage.setItem("curriculum", JSON.stringify(this.state.curriculum));
        this.props.changeType("view");
    }

    handleChange(e, fieldRef) {
        let new_curriculum = this.state.curriculum;
        new_curriculum[e.target.name] = e.target.value;

        let current_validation = this.state.validation;
        current_validation.form = this.formRef.current.checkValidity();
        current_validation[e.target.name] = fieldRef.current.validity;

        console.log(current_validation);
        this.setState({
            curriculum: new_curriculum,
            validation: current_validation
        });
    }

    render() {
        return (
            <>
                <form className="edit-form" ref={this.formRef} noValidate>
                    <div>
                        <label>Nome:</label><br/>
                        <input type="text" name="name" required value={this.state.curriculum.name} ref={this.nameRef} onChange={(e) => this.handleChange(e, this.nameRef)}/>
                        {
                            this.state.validation.name &&
                            !this.state.validation.name.valid &&
                            <div className="error-message">Nome é obrigatório</div>
                        }
                    </div>

                    <div>
                        <label>Email:</label><br/>
                        <input type="email" name="email" required value={this.state.curriculum.email} ref={this.emailRef} onChange={(e) => this.handleChange(e, this.emailRef)}/>
                        {
                            this.state.validation.email &&
                            this.state.validation.email.valueMissing &&
                            <div className="error-message">Email é obrigatório</div>
                        }
                    </div>

                    <div>
                        <label>Telefone/celular:</label><br/>
                        <input type="text" name="celphone" required value={this.state.curriculum.celphone} ref={this.celphoneRef} onChange={(e) => this.handleChange(e, this.celphoneRef)}/>
                        {
                            this.state.validation.celphone &&
                            !this.state.validation.celphone.valid &&
                            <div className="error-message">Telefone é obrigatório</div>
                        }
                    </div>

                    <div>
                        <label>Cargo de trabalho:</label><br/>
                        <input type="text" name="jobRole" required value={this.state.curriculum.jobRole} ref={this.jobRoleRef} onChange={(e) => this.handleChange(e, this.jobRoleRef)}/>
                        {
                            this.state.validation.jobRole &&
                            !this.state.validation.jobRole.valid &&
                            <div className="error-message">Cargo de trabalho é obrigatório</div>
                        }
                    </div>

                    <div>
                        <label>Descrição:</label><br/>
                        <textarea name="description" required value={this.state.curriculum.description} ref={this.descriptionRef} onChange={(e) => this.handleChange(e, this.descriptionRef)}/>
                        {
                            this.state.validation.description &&
                            !this.state.validation.description.valid &&
                            <div className="error-message">Descrição é obrigatória</div>
                        }
                    </div>

                    <Button value="Salvar" disabled={!this.state.validation.form} onClick={() => this.handleSave()}/>
                </form>
            </>
        )
    }
}

export default EditCurriculum