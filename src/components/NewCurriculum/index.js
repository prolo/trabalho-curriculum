import React from "react";
import './styles.css';

import Button from '../Button';

class NewCurriculum extends React.Component {

    render() {
        return (
            <>
                <Button onClick={() => this.props.changeType('edit')} value="Criar currículo"/>
            </>
        )
    }
}

export default NewCurriculum