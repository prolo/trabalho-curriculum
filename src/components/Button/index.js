import React from "react";
import './styles.css';

class Button extends React.Component {
    render() {
        return(
            <input className="button" type="button" onClick={this.props.onClick} disabled={this.props.disabled} value={this.props.value}/>        
        )
    }
}

export default Button