import React from "react";

import './App.css';

import NewCurriculum from "./components/NewCurriculum";
import ViewCurriculum from "./components/ViewCurriculum";
import EditCurriculum from "./components/EditCurriculum";

class App extends React.Component {
  constructor() {
    super()

    this.state = {
      type: 'new',
      curriculum: null
    }
  }

  componentDidMount() {
    let curriculum = localStorage.getItem("curriculum");
    if (curriculum) {
      this.setState({ curriculum: JSON.parse(curriculum) });
      this.changeType('view');
    }
  }

  changeType = (type) => { this.setState({ type })}
  setCurriculum = (curriculum) => { this.setState({ curriculum })}

  render() {
    return (
      <div className="app">
        { this.state.type === 'new' && <NewCurriculum changeType={this.changeType} /> }
        { this.state.type === 'view' && <ViewCurriculum changeType={this.changeType} curriculum={this.state.curriculum} /> }
        { this.state.type === 'edit' && <EditCurriculum changeType={this.changeType} curriculum={this.state.curriculum} setCurriculum={this.setCurriculum} /> }
      </div>
    )
  }
}

export default App