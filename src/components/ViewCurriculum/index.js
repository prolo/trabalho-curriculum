import React from "react";
import './styles.css';

import Button from '../Button';

class ViewCurriculum extends React.Component {

    render() {
        return (
            <div className="view-curriculum">
                <h1>{this.props.curriculum.name}</h1>
                <p>{this.props.curriculum.email}</p>
                <p>{this.props.curriculum.celphone}</p>
                <br/>
                <p>{this.props.curriculum.jobRole}</p>
                <br/>
                <p>{this.props.curriculum.description}</p>
                <br/>
                <div>
                    <Button onClick={() => {this.props.changeType("edit")}} value="Editar"/>
                </div>
            </div>
        )
    }
}

export default ViewCurriculum